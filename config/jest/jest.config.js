require('dotenv').config();

const path = require('path');
const dirs = require('../dirs');

const fileMock = path.resolve(__dirname, 'fileMock.js');
const cssMock = path.resolve(__dirname, 'cssMock.js');

module.exports = {
  rootDir: dirs.ROOT,
  testMatch: [
    '**/*.spec.(js|jsx)',
  ],
  setupTestFrameworkScriptFile: 'jest-enzyme',
  testEnvironment: 'enzyme',
  moduleNameMapper: {
    '.(gif|png|jpe?g|svg|woff|woff2|eot|ttf|otf)$': fileMock,
    '.(css|less)$': cssMock,
  },
  globals: {
    PRODUCTION: !!process.env.PRODUCTION,
    API_URL: process.env.API_URL,
  },
};
