import {
  TODO_CREATE_REQUEST,
  TODO_CREATE_SUCCESS,
  TODO_CREATE_FAILURE,
} from '../actions/todos';

const current = (
  state = {
    isCreating: false,
    errorMessage: null,
  },
  action,
) => {
  switch (action.type) {
    case TODO_CREATE_REQUEST:
      return { ...state, isCreating: true };
    case TODO_CREATE_SUCCESS:
      return { ...state, isCreating: false, errorMessage: null };
    case TODO_CREATE_FAILURE:
      return { ...state, isCreating: false, errorMessage: action.message };
    default:
      return state;
  }
};

export default current;

export const getError = state => state.errorMessage;

export const isCreating = state => state.isCreating;
