import { combineReducers } from 'redux';
import {
  SHOW_ALL,
  SHOW_COMPLETED,
  SHOW_ACTIVE,
} from '../constants/filters';
import byId, * as fromById from './byId';
import current, * as fromCurrent from './current';
import createList, * as fromList from './createList';

const idsByFilter = combineReducers({
  [SHOW_ALL]: createList(SHOW_ALL),
  [SHOW_ACTIVE]: createList(SHOW_ACTIVE),
  [SHOW_COMPLETED]: createList(SHOW_COMPLETED),
});

export default combineReducers({
  byId,
  idsByFilter,
  current,
});

export const getFilteredTodos = (
  state,
  filter,
) => {
  const ids = fromList.getIds(state.idsByFilter[filter]);
  return ids.map(id => fromById.getTodo(state.byId, id));
};

export const getIsFetching = (
  state,
  filter,
) => fromList.getIsFetching(state.idsByFilter[filter]);

export const getErrorMessage = (
  state,
  filter,
) => fromList.getErrorMessage(state.idsByFilter[filter]);

export const getTodoError = state => fromCurrent.getError(state.current);
export const getIsCreating = state => fromCurrent.isCreating(state.current);
