import {
  TODO_FETCH_SUCCESS, TODO_CREATE_SUCCESS, TODO_TOGGLE_SUCCESS,
} from '../actions/todos';

const byId = (state = {}, action) => {
  switch (action.type) {
    case TODO_FETCH_SUCCESS:
    case TODO_CREATE_SUCCESS:
    case TODO_TOGGLE_SUCCESS:
      return {
        ...state,
        ...action.todos.entities.todos,
      };
    default: return state;
  }
};

export default byId;

export const getTodo = (state, id) => state[id];
