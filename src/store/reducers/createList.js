import { combineReducers } from 'redux';
import {
  TODO_FETCH_REQUEST,
  TODO_FETCH_SUCCESS,
  TODO_FETCH_FAILURE,
  TODO_CREATE_SUCCESS,
  TODO_TOGGLE_SUCCESS,
} from '../actions/todos';
import {
  SHOW_COMPLETED, SHOW_ACTIVE,
} from '../constants/filters';

const createList = (filter) => {
  const handleToggle = (state, action) => {
    const { todos: { entities: { todos } }, id } = action;
    const todo = todos[id];
    const shouldRemove = (
      (todo.completed && filter === SHOW_ACTIVE)
      || (!todo.completed && filter === SHOW_COMPLETED)
    );
    return shouldRemove ? state.filter(stateId => stateId !== id)
      : state;
  };

  const ids = (state = [], action) => {
    switch (action.type) {
      case TODO_FETCH_SUCCESS:
        return filter === action.filter
          ? action.todos.result
          : state;
      case TODO_CREATE_SUCCESS:
        return filter === SHOW_COMPLETED
          ? state
          : [...state, action.todos.result];
      case TODO_TOGGLE_SUCCESS:
        return handleToggle(state, action);
      default: return state;
    }
  };

  const isFetching = (state = false, action) => {
    if (filter !== action.filter) return state;
    switch (action.type) {
      case TODO_FETCH_REQUEST:
        return true;
      case TODO_FETCH_FAILURE:
      case TODO_FETCH_SUCCESS:
        return false;
      default: return state;
    }
  };

  const errorMessage = (state = null, action) => {
    if (filter !== action.filter) return state;
    switch (action.type) {
      case TODO_FETCH_REQUEST:
      case TODO_FETCH_SUCCESS:
        return null;
      case TODO_FETCH_FAILURE:
        return action.message;
      default:
        return state;
    }
  };

  return combineReducers({
    ids,
    isFetching,
    errorMessage,
  });
};

export default createList;

export const getIds = state => state.ids;
export const getIsFetching = state => state.isFetching;
export const getErrorMessage = state => state.errorMessage;
