import deepFreeze from 'deep-freeze';
import todoApp, { getFilteredTodos, getIsCreating } from './todoApp';
import {
  SHOW_ALL,
  SHOW_ACTIVE,
  SHOW_COMPLETED,
} from '../constants/filters';
import {
  TODO_FETCH_SUCCESS,
  TODO_FETCH_REQUEST,
  TODO_FETCH_FAILURE,
  TODO_CREATE_REQUEST,
  TODO_CREATE_SUCCESS,
  TODO_CREATE_FAILURE,
  TODO_TOGGLE_SUCCESS,
} from '../actions/todos';
import * as ApiSchema from '../../api/schema';

describe('todoApp reducer', () => {
  it('creates state', () => {
    const expectedState = {
      byId: {},
      idsByFilter: {
        [SHOW_ALL]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_ACTIVE]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_COMPLETED]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
      },
      current: {
        isCreating: false,
        errorMessage: null,
      },
    };

    expect(todoApp(undefined, {})).toEqual(expectedState);
  });

  it('receives todos', () => {
    const stateBefore = todoApp(undefined, {});

    const stateAfter = {
      byId: {
        1: {
          id: 1,
          text: 'New Todo',
          completed: false,
        },
        0: {
          id: 0,
          text: 'Another Todo',
          completed: false,
        },
      },
      idsByFilter: {
        [SHOW_ALL]: {
          ids: [1, 0],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_ACTIVE]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_COMPLETED]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
      },
      current: {
        isCreating: false,
        errorMessage: null,
      },
    };

    const action = {
      type: TODO_FETCH_SUCCESS,
      todos: ApiSchema.arrayOfTodos([
        {
          id: 1,
          text: 'New Todo',
          completed: false,
        },
        {
          id: 0,
          text: 'Another Todo',
          completed: false,
        },
      ]),
      filter: SHOW_ALL,
    };

    deepFreeze(stateBefore);
    deepFreeze(action);
    expect(todoApp(stateBefore, action)).toEqual(stateAfter);
  });

  it('fetches todos', () => {
    const stateBefore = todoApp(undefined, {});
    const stateAfter = {
      byId: {},
      idsByFilter: {
        [SHOW_ALL]: {
          ids: [],
          isFetching: true,
          errorMessage: null,
        },
        [SHOW_ACTIVE]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_COMPLETED]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
      },
      current: {
        isCreating: false,
        errorMessage: null,
      },
    };

    const action = {
      type: TODO_FETCH_REQUEST,
      filter: SHOW_ALL,
    };
    deepFreeze(stateBefore);
    deepFreeze(action);
    expect(todoApp(stateBefore, action)).toEqual(stateAfter);
  });

  it('sets error message', () => {
    const stateBefore = todoApp(undefined, {});
    const stateAfter = {
      byId: {},
      idsByFilter: {
        [SHOW_ALL]: {
          ids: [],
          isFetching: false,
          errorMessage: 'error',
        },
        [SHOW_ACTIVE]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_COMPLETED]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
      },
      current: {
        isCreating: false,
        errorMessage: null,
      },
    };

    const action = {
      type: TODO_FETCH_FAILURE,
      filter: SHOW_ALL,
      message: 'error',
    };
    deepFreeze(stateBefore);
    deepFreeze(action);
    expect(todoApp(stateBefore, action)).toEqual(stateAfter);
  });

  it('resets error after failure', () => {
    const requestAction = {
      type: TODO_FETCH_REQUEST,
      filter: SHOW_ALL,
    };

    const failureAction = {
      type: TODO_FETCH_FAILURE,
      filter: SHOW_ALL,
      message: 'error',
    };

    const listAfterRequest = {
      ids: [],
      isFetching: true,
      errorMessage: null,
    };

    const listAfterFailure = {
      ids: [],
      isFetching: false,
      errorMessage: 'error',
    };

    const stateBeforeRequest = todoApp(undefined, {});
    const stateAfterRequest = todoApp(stateBeforeRequest, requestAction);
    const stateAfterFailure = todoApp(stateAfterRequest, failureAction);

    expect(stateAfterRequest.idsByFilter[SHOW_ALL]).toEqual(listAfterRequest);
    expect(stateAfterFailure.idsByFilter[SHOW_ALL]).toEqual(listAfterFailure);
  });

  it('requests todo creation', () => {
    const stateBefore = todoApp(undefined, {});
    const stateAfter = {
      byId: {},
      idsByFilter: {
        [SHOW_ALL]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_ACTIVE]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_COMPLETED]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
      },
      current: {
        isCreating: true,
        errorMessage: null,
      },
    };

    const action = {
      type: TODO_CREATE_REQUEST,
    };

    deepFreeze(action);
    deepFreeze(stateBefore);
    expect(todoApp(stateBefore, action)).toEqual(stateAfter);
  });

  it('saves todo after todo creation request', () => {
    const stateBefore = todoApp(undefined, {});
    const stateAfter = {
      byId: {
        1: {
          id: 1,
          text: 'New Todo',
          completed: false,
        },
      },
      idsByFilter: {
        [SHOW_ALL]: {
          ids: [1],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_ACTIVE]: {
          ids: [1],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_COMPLETED]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
      },
      current: {
        isCreating: false,
        errorMessage: null,
      },
    };

    const action = {
      type: TODO_CREATE_SUCCESS,
      todos: ApiSchema.todo({
        id: 1,
        text: 'New Todo',
        completed: false,
      }),
    };

    deepFreeze(action);
    deepFreeze(stateBefore);
    expect(todoApp(stateBefore, action)).toEqual(stateAfter);
  });

  it('sets error on todo creation failure', () => {
    const stateBefore = todoApp(undefined, {});
    const stateAfter = {
      byId: {},
      idsByFilter: {
        [SHOW_ALL]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_ACTIVE]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_COMPLETED]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
      },
      current: {
        isCreating: false,
        errorMessage: 'some error',
      },
    };
    const action = {
      type: TODO_CREATE_FAILURE,
      message: 'some error',
    };

    deepFreeze(stateBefore);
    deepFreeze(stateAfter);

    expect(todoApp(stateBefore, action)).toEqual(stateAfter);
  });

  it('toggles todo', () => {
    const addTodoAction = {
      type: TODO_CREATE_SUCCESS,
      todos: ApiSchema.todo({
        id: 1,
        text: 'hello',
        completed: false,
      }),
    };
    const toggleTodo = {
      type: TODO_TOGGLE_SUCCESS,
      id: 1,
      todos: ApiSchema.todo({
        id: 1,
        text: 'hello',
        completed: true,
      }),
    };

    const expectedState = {
      byId: {
        1: {
          id: 1,
          text: 'hello',
          completed: true,
        },
      },
      idsByFilter: {
        [SHOW_ALL]: {
          ids: [1],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_ACTIVE]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
        [SHOW_COMPLETED]: {
          ids: [],
          isFetching: false,
          errorMessage: null,
        },
      },
      current: {
        isCreating: false,
        errorMessage: null,
      },
    };

    const stateBefore = todoApp(undefined, {});
    const stateWithTodo = todoApp(stateBefore, addTodoAction);

    deepFreeze(toggleTodo);
    deepFreeze(expectedState);

    expect(todoApp(stateWithTodo, toggleTodo)).toEqual(expectedState);
  });
});


describe('getFiltredTodos selector', () => {
  it('returns all todos', () => {
    const createAction = {
      type: TODO_CREATE_SUCCESS,
      todos: ApiSchema.todo({
        id: 1,
        text: 'New Todo',
        completed: false,
      }),
    };

    const firstTodo = todoApp(undefined, createAction);

    createAction.todos = ApiSchema.todo({
      id: 0,
      text: 'Another Todo',
      completed: false,
    });

    const secondTodo = todoApp(firstTodo, createAction);

    const result = [
      {
        id: 1,
        text: 'New Todo',
        completed: false,
      },
      {
        id: 0,
        text: 'Another Todo',
        completed: false,
      },
    ];

    deepFreeze(firstTodo);
    deepFreeze(secondTodo);
    deepFreeze(createAction);

    expect(getFilteredTodos(secondTodo, SHOW_ALL)).toEqual(result);
  });
});

describe('getIsCreationg selector', () => {
  it('returns true on todo create request', () => {
    const stateBefore = todoApp(undefined, {});
    const action = {
      type: TODO_CREATE_REQUEST,
    };

    const stateAfter = todoApp(stateBefore, action);

    deepFreeze(stateAfter);

    expect(getIsCreating(stateAfter)).toEqual(true);
  });
});
