import * as endpoints from '../../api/endpoints';
import { getIsFetching } from '../reducers/todoApp';
import * as ApiSchema from '../../api/schema';

export const TODO_FETCH_REQUEST = 'TODO_FETCH_REQUEST';
export const TODO_FETCH_SUCCESS = 'TODO_FETCH_SUCCESS';
export const TODO_FETCH_FAILURE = 'TODO_FETCH_FAILURE';

export const fetchTodos = filter => (dispatch, getState) => {
  if (getIsFetching(getState(), filter)) return Promise.resolve();

  dispatch({ type: TODO_FETCH_REQUEST, filter });

  return endpoints.getTodos(filter)
    .then(
      todos => dispatch({
        type: TODO_FETCH_SUCCESS,
        filter,
        todos: ApiSchema.arrayOfTodos(todos),
      }),
      error => dispatch({
        type: TODO_FETCH_FAILURE,
        filter,
        message: error.message,
      }),
    );
};


export const TODO_CREATE_REQUEST = 'TODO_CREATE_REQUEST';
export const TODO_CREATE_SUCCESS = 'TODO_CREATE_SUCCESS';
export const TODO_CREATE_FAILURE = 'TODO_CREATE_FAILURE';

const addTodoRequest = () => ({
  type: TODO_CREATE_REQUEST,
});

const receiveTodo = todos => ({
  type: TODO_CREATE_SUCCESS,
  todos,
});

const todoAddFailure = message => ({
  type: TODO_CREATE_FAILURE,
  message,
});

export const createTodo = text => (dispatch) => {
  dispatch(addTodoRequest());
  endpoints
    .createTodo(text)
    .then(
      todo => dispatch(receiveTodo(ApiSchema.todo(todo))),
      error => dispatch(todoAddFailure(error.message)),
    );
};

export const TODO_TOGGLE_REQUEST = 'TODO_TOGGLE_REQUEST';
export const TODO_TOGGLE_SUCCESS = 'TODO_TOGGLE_SUCCESS';
export const TODO_TOGGLE_FAILURE = 'TODO_TOGGLE_FAILURE';

const toggleSuccess = (id, todos) => ({
  type: TODO_TOGGLE_SUCCESS,
  todos,
  id,
});

export const toggleTodo = (id, completed) => (dispatch) => {
  endpoints
    .toggleTodo(id, completed)
    .then(todo => dispatch(toggleSuccess(id, ApiSchema.todo(todo))));
};
