import { createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import todoApp from './reducers/todoApp';

const configureStore = () => {
  const middlewares = [thunk];

  if (!PRODUCTION) middlewares.push(logger);

  return createStore(
    todoApp,
    composeWithDevTools(
      applyMiddleware(...middlewares),
    ),
  );
};

export default configureStore;
