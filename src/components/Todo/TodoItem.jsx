import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const TodoItem = ({
  text,
  completed,
  onToggle,
}) => (
  <li>
    <h2
      className={classNames('todoText', { completed })}
    >
      {text}
    </h2>
    <button
      type="button"
      onClick={onToggle}
    >
      Toggle
    </button>
  </li>
);

TodoItem.propTypes = {
  text: PropTypes.string.isRequired,
  completed: PropTypes.bool.isRequired,
  onToggle: PropTypes.func.isRequired,
};

export default TodoItem;
