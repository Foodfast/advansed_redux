import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import {
  getFilteredTodos,
  getIsFetching,
  getErrorMessage,
} from '../../store/reducers/todoApp';
import * as actions from '../../store/actions/todos';
import TodoList from './TodoList';
import TodoError from './TodoError';

class VisibleTodos extends Component {
  componentDidMount() {
    this.getData();
  }

  componentDidUpdate(prevProps) {
    const { filter } = this.props;
    if (filter !== prevProps.filter) this.getData();
  }

  getData() {
    const { filter, fetchTodos } = this.props;
    fetchTodos(filter);
  }

  render() {
    const {
      toggleTodo,
      todos,
      isFetching,
      errorMessage,
    } = this.props;

    if (!todos.length && isFetching) {
      return <p>Loading...</p>;
    }

    if (!todos.length && errorMessage) {
      return (
        <TodoError
          message={errorMessage}
          onRetry={() => this.getData()}
        />
      );
    }

    return (
      <TodoList
        todos={todos}
        onTodoToggle={toggleTodo}
      />
    );
  }
}

VisibleTodos.propTypes = {
  toggleTodo: PropTypes.func.isRequired,
  fetchTodos: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired,
  todos: PropTypes.arrayOf(Object).isRequired,
  filter: PropTypes.string.isRequired,
  errorMessage: PropTypes.string,
};

VisibleTodos.defaultProps = {
  errorMessage: 'Oops!',
};

const mapStateToProps = (
  state,
  { match: { params: { filter } } },
) => ({
  filter,
  todos: getFilteredTodos(state, filter),
  isFetching: getIsFetching(state, filter),
  errorMessage: getErrorMessage(state, filter),
});

export default withRouter(connect(
  mapStateToProps,
  actions,
)(VisibleTodos));
