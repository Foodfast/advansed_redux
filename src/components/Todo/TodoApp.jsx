import React from 'react';
import { hot } from 'react-hot-loader';
import PropTypes from 'prop-types';
import './TodoApp.css';
import TodoControls from './TodoControls';
import TodoFilters from './TodoFilters';
import VisibleTodos from './VisibleTodos';

const TodoApp = () => (
  <div>
    <VisibleTodos />
    <TodoControls />
    <TodoFilters />
  </div>
);

TodoApp.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.object.isRequired,
  }).isRequired,
};

export default hot(module)(TodoApp);
