import React from 'react';
import PropTypes from 'prop-types';

const TodoError = ({
  message,
  onRetry,
}) => (
  <div>
    <p>
      Error:
      {message}
    </p>
    <button type="button" onClick={onRetry}>Rerty</button>
  </div>
);

TodoError.propTypes = {
  message: PropTypes.string.isRequired,
  onRetry: PropTypes.func.isRequired,
};

export default TodoError;
