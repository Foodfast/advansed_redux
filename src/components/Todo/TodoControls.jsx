import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createTodo } from '../../store/actions/todos';
import { getIsCreating } from '../../store/reducers/todoApp';

const TodoControls = ({
  onAddTodo,
  isCreating,
}) => {
  let input;

  const add = () => {
    onAddTodo(input.value);
    input.value = '';
    input.focus();
  };

  return (
    <div>
      <input
        type="text"
        ref={(node) => { input = node; }}
      />
      <button
        type="button"
        onClick={add}
        disabled={isCreating}
      >
        {isCreating ? 'Creating...' : 'Add todo'}
      </button>
    </div>
  );
};

TodoControls.propTypes = {
  onAddTodo: PropTypes.func.isRequired,
  isCreating: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  isCreating: getIsCreating(state),
});

const mapDispatchToProps = dispatch => ({
  onAddTodo(text) { dispatch(createTodo(text)); },
});

export default connect(mapStateToProps, mapDispatchToProps)(TodoControls);
