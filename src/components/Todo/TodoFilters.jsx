import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import {
  SHOW_ALL,
  SHOW_ACTIVE,
  SHOW_COMPLETED,
} from '../../store/constants/filters';

const VisibilityFilter = ({ filter, children }) => (
  <NavLink
    to={`/${filter}`}
    exact
    activeStyle={{
      fontStyle: 'bold',
      color: 'red',
    }}
  >
    {children}
  </NavLink>
);

VisibilityFilter.propTypes = {
  filter: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

const TodoFilters = () => (
  <div>
    <p>Show: </p>
    <ul>
      <li>
        <VisibilityFilter filter={SHOW_ALL}>
          Show all
        </VisibilityFilter>
      </li>
      <li>
        <VisibilityFilter filter={SHOW_ACTIVE}>
          Show active
        </VisibilityFilter>
      </li>
      <li>
        <VisibilityFilter filter={SHOW_COMPLETED}>
          Show completed
        </VisibilityFilter>
      </li>
    </ul>
  </div>
);

export default TodoFilters;
