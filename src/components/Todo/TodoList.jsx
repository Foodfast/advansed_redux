import React from 'react';
import PropTypes from 'prop-types';
import TodoItem from './TodoItem';

const TodoList = ({
  todos,
  onTodoToggle,
}) => (
  <ol>
    {todos.map(t => (
      <TodoItem
        key={t.id}
        {...t}
        onToggle={() => onTodoToggle(t.id, !t.completed)}
      />
    ))}
  </ol>
);

TodoList.propTypes = {
  todos: PropTypes.arrayOf(Object).isRequired,
  onTodoToggle: PropTypes.func.isRequired,
};

export default TodoList;
