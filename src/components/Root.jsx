import React from 'react';
import { Provider } from 'react-redux';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from 'react-router-dom';
import TodoApp from './Todo/TodoApp';
import configureStore from '../store/store';
import {
  SHOW_ALL,
} from '../store/constants/filters';

const store = configureStore();

const Root = () => (
  <Provider store={store}>
    <Router>
      <Switch>
        <Redirect exact from="/" to={`/${SHOW_ALL}`} />
        <Route path="/:filter" component={TodoApp} />
      </Switch>
    </Router>
  </Provider>
);

export default Root;
