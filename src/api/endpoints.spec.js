import {
  getTodos,
} from './endpoints';

jest.mock('./endpoints');

describe('todos', () => {
  it('returns todos', () => (
    getTodos().then((todos) => {
      const t = todos[0];

      expect(todos).toBeInstanceOf(Array);
      expect(t).toHaveProperty('id');
      expect(t).toHaveProperty('completed');
      expect(t).toHaveProperty('text');
    })
  ));
});
