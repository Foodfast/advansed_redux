module.exports = (_, res, next) => {
  if (Math.random() >= 0.5) res.status(500);
  next();
};
