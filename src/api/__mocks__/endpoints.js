import data from './data';

const { todos } = data();

const endpointsMock = {
  getTodos: jest.fn(() => Promise.resolve(todos)),
};

module.exports = PRODUCTION
  ? jest.requireActual('../endpoints')
  : endpointsMock;
