const data = {
  todos: [
    {
      id: 1,
      text: 'testing',
      completed: false,
    },
    {
      id: 2,
      text: 'another todo',
      completed: false,
    },
  ],
};

module.exports = () => data;
