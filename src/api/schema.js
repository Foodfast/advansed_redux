import { normalize, schema } from 'normalizr';

const todoEntity = new schema.Entity('todos');
const todos = [todoEntity];
export const todo = response => normalize(response, todoEntity);
export const arrayOfTodos = res => normalize(res, todos);
