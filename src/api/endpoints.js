import axios from 'axios';
import {
  SHOW_ALL,
  SHOW_COMPLETED,
  SHOW_ACTIVE,
} from '../store/constants/filters';

const apiClinet = axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
  },
});


export const TODOS_ENDPOINT = '/todos';
export const getTodos = (filter) => {
  const getFilterString = () => {
    switch (filter) {
      case SHOW_ALL: return '';
      case SHOW_COMPLETED: return 'completed=true';
      case SHOW_ACTIVE: return 'completed=false';
      default: return '';
    }
  };

  const filterString = getFilterString(filter);
  return apiClinet.get(`${TODOS_ENDPOINT}?${filterString}`)
    .then(response => response.data);
};

export const createTodo = text => (
  apiClinet.post(TODOS_ENDPOINT, {
    text,
    completed: false,
  }).then(response => response.data)
);

export const toggleTodo = (id, completed) => (
  apiClinet.patch(`${TODOS_ENDPOINT}/${id}`, {
    completed,
  }).then(response => response.data)
);
